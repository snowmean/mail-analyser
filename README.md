# README #

### What is mail analyser? ###
Mail analyser is simple and extendible tool for checking incoming e-mails, written on pure Ruby with minimal amount of ruby modules needed.
The main script ma.rb was made specially to work with postfix server as mailbox_command script, and using all variables from postfix.
The script just collecting all data from postfix, save it to the data.yaml files into data folder, and start tests from modules. After it, all test results are writing to the result.yaml file.

### How do I get set up? ###
Just install postfix, and add line to config
```
#!ruby
mailbox_command = /path/to/mail-analyser/ma.rb
```

### How to write modules? ###
All modules are simple files with name like <module_name>.test.rb in modules folder. Inside module is only the function with the same name as module_name. Function may use global data variable, and require some additional gems. The function should return Hash with format:
```
#!ruby
{:result => "OK || FAIL", :comment => "Some comment here"}
```
All results from modules will be collected into global result variable in format like this
```
#!ruby
@result = {
    :module_name => {
         :result => "OK",
         :comment => "Some comment",
     },
    :another_module_name => {
         :result => "FAIL",
         :comment => "Fail to detect something",
     }
}
```
As you can see it's not difficult structure. Anyway, all results will be saved to the file, and will be available to the further manipulations.