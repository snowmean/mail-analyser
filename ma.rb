#!/usr/bin/ruby

require 'mail'
require 'yaml'
require 'logger'

# Piece of logs
@log = Logger.new "/tmp/mail-analyzer.log"

if ARGV[1] == "test"
    @root_dir = "."
else
    @root_dir = "/srv/mail-analyzer"
end

@time = Time.now
@result = Hash.new
@modules = Array.new

@log.info "Loading modules"
Dir["#{@root_dir}/modules/*.test.rb"].each do |file|
    @log.info "Loading module from file: #{file}"
    require file
    @modules.push(File.basename(file, ".test.rb"))
end

@data = Hash.new

if ARGV[1] == "test"
    @data = YAML::load_file("#{@root_dir}/data/data.yaml")
else
# Sender mail address
@data = {
    # Remote client network address.
    :client_address     => ENV["CLIENT_ADDRESS"],
    # Remote client EHLO command parameter.
    :client_hello       => ENV["CLIENT_HELO"],
    # Remote client hostname.
    :client_hostname    => ENV["CLIENT_HOSTNAME"],
    # Remote client protocol.
    :client_protocol    => ENV["CLIENT_PROTOCOL"],
    # The domain part of the recipient address.
    :domain             => ENV["DOMAIN"],
    # The optional address extension.
    :extension          => ENV["EXTENSION"],
    # The recipient home directory.
    :home               => ENV["HOME"],
    # The recipient address localpart.
    :local              => ENV["LOCAL"],
    # The recipient's username.
    :logname            => ENV["LOGNAME"],
    # The entire recipient address, before any address rewriting or aliasing.
    :original_recipient => ENV["ORIGINAL_RECIPIENT"],
    # The full recipient address.
    :recipient          => ENV["RECIPIENT"],
    # SASL authentication method specified in the remote client AUTH command.
    :sasl_method        => ENV["SASL_METHOD"],
    # SASL sender address specified in the remote client MAIL FROM command. 
    :sasl_sender        => ENV["SASL_SENDER"],
    # SASL username specified in the remote client AUTH command. 
    :sasl_user          => ENV["SASL_USER"],
    # The full sender address.
    :sender             => ENV["SENDER"],
    # The recipient's login shell.
    :shell              => ENV["SHELL"],
    #The recipient username.
    :user               => ENV["USER"],
    # Message body
    :body               => "",
}
end

# Receiving mail body
while line = $stdin.gets
    @data[:body] += line
end

# Save message and variables to file
filename = "#{@root_dir}/data/#{@data[:sender]}-#{@time.year}#{@time.month}#{@time.day}#{@time.hour}#{@time.min}.data.yaml"
@log.info "Writing data to file: #{filename}"
File.open(filename, "w+") do |file|
    file.write(@data.to_yaml)
end

# Run tests
@log.info "Running tests"
@modules.each do |test|
    begin
        @log.info "Test: #{test}"
        @result[test.to_sym] = send test
    rescue => e
        @log.error "Test #{test} is failed: #{e.message}"
    end
end

# Save tests result
filename = "#{@root_dir}/data/#{@data[:sender]}-#{@time.year}#{@time.month}#{@time.day}#{@time.hour}#{@time.min}.result.yaml"
@log.info "Writing results to file: #{filename}"
File.open(filename, "w+") do |file|
    file.write(@result.to_yaml)
end