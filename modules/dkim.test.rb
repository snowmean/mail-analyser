# Function
def dkim
    result = Hash.new
    mail = Mail.read_from_string @data[:body]
    if mail.header["DKIM-Signature"].nil?
        result[:result] = "FAIL"
        result[:comment] = "No DKIM signature found in file"
    else
        dkim_domain = Hash[*mail.header["DKIM-Signature"].field.to_s.split("; ").map{|i| i.split("=")}.flatten]["d"]
        if dkim_domain == @data[:sender].split("@")[1]
            result[:result] = "OK"
            result[:comment] = "DKIM signaure configured"
        else
            result[:result] = "FAIL"
            result[:comment] = "DKIM signature domain exits for #{dkim_domain}, but message sent from #{@data[:sender].split('@')[1]}"
        end
    end
    # Return ready to use data
    @log.info "DKIM test #{result[:result]}: #{result[:comment]}"
    return result
end